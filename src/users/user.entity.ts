import { IsEmail } from "class-validator";
import { Report } from "src/reports/report.entity";
import { AfterUpdate, AfterRemove, AfterInsert, Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";


@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({ default: true })
    admin: boolean

    @OneToMany(() => Report, (report) => report.user)
    reports: Report[];


    @AfterInsert()
    logInsert() {
        console.log('Inserted user with id' + this.id)
    }

    @AfterUpdate()
    logUpdate() {
        console.log('updated user with id' + this.id)
    }
    @AfterRemove()
    logRemove() {
        console.log('Removed user with id' + this.id)
    }

}